'''
Created on Jun 24, 2011

@author: juanlmateo
'''

# aggregate xls peak files from MACS 2

import sys, getopt, glob

def usage():
    print('Usage: aggregatePeaksFromSubsampling [OPTIONS]')
    print('--prefix=[string]')
    print('--suffix=[string]')
    print('--nostrict\tThe peaks do not need to overlap totally.[DEFAULT]')
    print('--strict\tThe peaks do need to overlap totally.')

def getMedian(array):
    array.sort()
    center = len(array) / 2
    if(len(array) % 2 == 0):
        return (array[center] + array[center - 1]) / 2.0
    else:
        return array[center]

def isOverlapping(locus, start, end):
    if locus[0] >= start and locus[0] <= end:
        return True
    if locus[1] >= start and locus[1] <= end:
        return True
    if start >= locus[0] and start <= locus[1]:
        return True
    return False

def testIdentical(array):
    for i in range(len(array) - 1):
        if (array[i] != array[i + 1]):
            return False
    return True

class Peak:
    pass

def main(argv):
    try:                                
        opts, args = getopt.getopt(argv, '', ['prefix=', 'suffix=', 'nostrict', 'strict'])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    prefix = None
    sufix = None
    noStrict = True
    for opt, arg in opts:
        if opt == '--prefix':                   
            prefix = arg
        if opt == '--suffix':                   
            sufix = arg
        if opt == '--nostrict':
            noStrict = True
        if opt == '--strict':
            noStrict = False
            
    if (prefix is None or sufix is None):
        usage()
        sys.exit(2)
    
    output = sys.__stdout__
    output.write('chr\tstart\tend\tlength\tabs_summit\tpileup\t-log10(pvalue)\tfold_enrichment\t-log10(qvalue)\n')
    
    fileNames = glob.glob(prefix + '*' + sufix)
    loci = {}
    for fileName in fileNames:
        inFile = open(fileName, 'r')
        for line in inFile:
            
            if (line.startswith('#')) or (line.startswith('chr\t') or len(line) < 3):
                #output.write(line)
                continue
            if (not line.startswith('chr')):
                output.write(line)
                continue
            line = line.strip()
            newPeak = Peak()
            cols = line.split('\t')
            newPeak.chrom = cols[0]
            newPeak.start = int(cols[1])
            newPeak.end = int(cols[2])
            newPeak.summit = int(cols[4])
            newPeak.pileUp = float(cols[5])
            newPeak.pScore = float(cols[6])
            newPeak.fe = float(cols[7])
            newPeak.qScore = float(cols[8])
            
            loci.setdefault(newPeak.chrom, [])
            
            included = False
            for locus in loci[newPeak.chrom]:
                if noStrict:
                    # it is necessary to check that the summit of all previous peaks is included in this peak coordinates and vice versa
                    accepted = True
                    for peak in locus:
                        if(newPeak.summit > peak.end or newPeak.summit < peak.start or peak.summit > newPeak.end or peak.summit < newPeak.start):
                            accepted = False
                            break
                    if accepted:
                        locus.append(newPeak)
                        included = True
                else:
                    if(newPeak.summit == locus[0].summit):
                        locus.append(newPeak)
                        included = True
            if not included:
                loci[newPeak.chrom].append([newPeak])
        inFile.close()
            

    chromosomes = loci.keys()
    chromosomes.sort()
    for chrom in chromosomes:
        for locus in loci[chrom]:
            if len(locus) > 8:
                start = 0
                end = 1e10
                pileUp = []
                pScore = []
                fe = []
                qScore = []
                summits = []
                for peak in locus:
                    if(peak.start > start):
                        start = peak.start
                    if(peak.end < end):
                        end = peak.end
                    pileUp.append(peak.pileUp)
                    pScore.append(peak.pScore)
                    fe.append(peak.fe)
                    qScore.append(peak.qScore)
                    summits.append(peak.summit)
                if(noStrict):
                    summit = getMedian(summits)
                else:
                    summit = locus[0].summit
                output.write('%s\t%s\t%s\t%s\t%s\t%.2f\t%.2f\t%.2f\t%.2f\n' % (chrom, str(start), str(end), str(end - start + 1), int(summit), getMedian(pileUp), getMedian(pScore), getMedian(fe), getMedian(qScore)))

if __name__ == '__main__':
    main(sys.argv[1:])