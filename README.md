# Mateo J.L. et al. Genome Research 2015

Python scripts used in the publication Mateo J.L. et al. Genome Research 2015
http://genome.cshlp.org/content/25/1/41.full

These scripts were developed with Python 2.7 and the script balanceBAMFiles.py requires the [pysam](https://github.com/pysam-developers/pysam) module.